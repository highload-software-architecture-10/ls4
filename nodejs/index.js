const GA_API_SECRET = process.env.GA_API_SECRET;
const GA_MEASUREMENT_ID = process.env.GA_MEASUREMENT_ID;
const ga4mp = require('ga4-mp');
const ga4 = ga4mp.createClient(GA_API_SECRET, GA_MEASUREMENT_ID, "680631999932-47f9nvojk2usbm6qui0tatj1kimlchs8.apps.googleusercontent.com");

console.log('GA_API_SECRET', GA_API_SECRET, GA_MEASUREMENT_ID);

async function update() {
    const rate = await getRate();
    await sendRate(rate);
    console.log('update', rate);
}

async function getRate() {
    const data = await fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&json').then(res => res.json());

    return data[0].rate;
}

async function sendRate(rate) {
    // const data = await fetch(`https://www.google-analytics.com/mp/collect?measurement_id=${GA_MEASUREMENT_ID}&api_secret=${GA_API_SECRET}`,
    //     {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             client_id: '000000000.0000000000',
    //             events: [{
    //                 name: 'UAH/USD',
    //                 params: {
    //                     value: rate
    //                 }
    //             }]
    //         })
    //     }
    //     ).then(res => res.status);

    ga4.send([
        {
            name : 'rate',
            params : {
                currency : 'USD',
                value : rate
            }
        },
    ]);
    console.log('sendRate');
}

async function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

(async () => {
    while (true) {
        await update();
        await delay(1000);
    }
})()